Personality Drug Analysis
==============================

This repository hosts the code and data for analyzing the relationship between personality traits and drug consumption patterns. The dataset contains records from 1885 respondents, including personality measurements based on NEO-FFI-R, BIS-11, and ImpSS scales, as well as information on education level, age, gender, country of residence, ethnicity, and drug usage history

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
